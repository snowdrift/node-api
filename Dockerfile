# syntax=docker/dockerfile:1
#FROM node:18-alpine as base
FROM mongo:latest as base

FROM base as builder

WORKDIR /home/node
COPY package*.json ./

COPY . .
RUN npm install
RUN npm build

FROM base as runtime

ENV NODE_ENV=production

WORKDIR /home/node
COPY package*.json  ./

RUN npm install --production
COPY --from=builder /home/node/dist ./dist
COPY --from=builder /home/node/build ./build

EXPOSE 3000

CMD ["node", "dist/server.js"]

