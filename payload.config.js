import {buildConfig} from 'payload/config';
import Users from './collections/Users';
import Projects from './collections/Projects';

export default buildConfig({
  // By default, Payload will boot up normally
  // and you will be provided with a base `User` collection.
  // But, here is where you define how you'd like Payload to work!
  cors: [ // whitelist of domains to allow CORS requests from
  'https://localhost:1234',
  'https://localhost:3000',
  'http://localhost:1234',
  'http://localhost:3000',
  'https://snowdrift.coop',
  'https://new.snowdrift.coop',
  ],
  csrf: [ // whitelist of domains to allow cookie auth from
  'https://localhost:1234',
  'https://localhost:3000',
  'http://localhost:1234',
  'http://localhost:3000',
  'https://snowdrift.coop',
  'https://new.snowdrift.coop',
  ],

  // rateLimits provide basic API DDOS (Denial-of-service) protection and can limit accidental server load from scripts
  rateLimit: {
    trustProxy: true,
    window: 2 * 60 * 1000, // 2 minutes
    max: 2400, // limit each IP per windowMs
  },

  // collections in Payload are synonymous with database tables, models or entities from other frameworks and systems
  collections: [
    Users,
    Projects
  ],

  admin: {
    avatar: 'gravatar',
    meta: {
      titleSuffix: "- Snowdrift Platform",
      favicon: "https://snowdrift.coop/favicon.ico", // TODO make backend-specific favicon
      ogImage: "https://snowdrift.coop/static/img/main/navbar-logo.png" // Image that will appear in the preview when you share links to your admin panel online and through social media.
    }
  },
});
