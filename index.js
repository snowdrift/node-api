const express = require('express');
const payload = require('payload');
const pg = require('pg');
const { Client } = pg;

require('dotenv').config({
  path: require('path').resolve(__dirname, './.env'),
});

const app = express();
const db = new Client();

// Redirect all traffic at root to admin UI
app.get('/', function (_, res) {
  res.redirect('/admin');
});




// router.get('/', (req, res) => {
//   if (req.user) {
//     return res.send(`Authenticated successfully as ${req.user.email}.`);
//   }

//   return res.send('Not authenticated');
// });


// router.get('/p/snowdrift/pledge_count', async (req, res) => {
//     let result = await db.query('SELECT COUNT(*) from crowdmatch__patron WHERE pledge_since IS NOT NULL');
//     console.log(result.rows[0].count);
//     res.send(result.rows[0].count)
// })

// router.get('/am_i_pledged', async (req, res) => {
//     if (req.user) {
//         let result = await db.query('SELECT p.pledge_since FROM crowdmatch__patron p JOIN usr ON usr.id = p.usr WHERE usr.email = $1', [req.user.email]);
//         return res.send(result.rows.length === 1 ? `Pledged since ${result.rows[0].pledge_since}` : 'Not pledged');
//     }
//     return res.send('Not pledged');
// });



// router.put('/p/snowdrift/pledge', (req, res) => {
//   res.send('hello world')
// })
// router.delete('/p/snowdrift/pledge', (req, res) => {
//   res.send('hello world')
// })
// router.put('/user/payment/stripe', (req, res) => {
//   res.send('hello world')
// })
// router.delete('/user/payment/stripe', (req, res) => {
//   res.send('hello world')
// })


const start = async () => {
  // TODO Postgres init
  // await db.connect();

  const router = express.Router();
  

	// Initialize Payload
	await payload.init({
    secret: process.env.PAYLOAD_SECRET,
    mongoURL: process.env.MONGODB_URL,
    express: app,
    telemetry: false,
    onInit: async () => {
      payload.logger.info(`Payload Admin URL: ${payload.getAdminURL()}`);
      // TODO seed with demo data here
    },
  });

  router.use(payload.authenticate);




  app.use('/router', router);
	app.listen(3000);
}

start();
