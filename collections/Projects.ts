import { CollectionConfig, FieldHook } from 'payload/types';
import { isAdmin } from "../access/isAdmin";
import { isAdminOrProjectAdmin } from "../access/isAdminOrProjectAdmin";
import { isLoggedIn } from '../access/isLoggedIn';
import formatSlug from '../helpers/slugify';
// import Content from '../blocks/Content';
// import { Media } from '../blocks/Media';
// import MediaContent from '../blocks/MediaContent';
// import MediaSlider from '../blocks/MediaSlider';



const Projects: CollectionConfig = {
  // the slug is used for naming the collection in the database and the APIs that are open. For example: api/posts/${id}
  slug: 'projects',
  admin: {
    // this is the name of a field which will be visible for the edit screen and is also used for relationship fields
    useAsTitle: 'title',
    // defaultColumns is used on the listing screen in the admin UI for the collection
    defaultColumns: [
      'title',
      'publishDate',
      'tags',
      'status'
    ],
    group: 'Content'
  },
  access: {
    // Anyone can create a draft project, though only global admins can make them public for now
    create: isLoggedIn,
    // Only global admins or project admins can update
    update: isAdminOrProjectAdmin('id'),
    // Only admins can delete, for now
    delete: isAdmin,

    read: ({ req: { user, doc } }) => {
      if (user.roles.includes('admin')) return true;
      // users who are authenticated will see all projects
      
      // TODO if project admin, editor, or author, allow access

      // query publishDate to control when projects are visible to guests
      return {
        _status: {
          equals: 'published',
        },
      };
    },
  },
  // versioning with drafts enabled tells Payload to save documents to a separate collection in the database and allow publishing
  versions: {
    drafts: true,
  },
  fields: [
    { // example "Snowdrift.coop: Crowdmatching platform"
      name: 'title',
      label: "Project Title",
      type: 'text',
      required: true,
      // localized fields are stored as keyed objects to represent each locale listed in the payload.config.ts. For example: { en: 'English', es: 'Espanol', ...etc }
      localized: true,
      admin: {
        description: "The title will be shown everywhere your project is represented",
      }
    },
    { // example "Snowdrift.coop: Crowdmatching platform"
      name: 'slug',
      label: "Slug (name in URL)",
      type: 'text',
      required: true,
      unique: true, // slug can't be already used by another project
      minLength: 3,
      defaultValue: formatSlug('title'),// TODO generate default value based on title
      admin: {
        description: "The slug will form the URL for this project's page. It must consist of only lower-case letters, numbers, and hyphens. Use hyphens to separate words. Minimum 3 characters.", 
      }, // TODO enforce this constraint, as well as blocking reserved pages such as "about"
      hooks: {
        beforeValidate: [
          formatSlug('title'),
        ],
      },
    },
    { // example "The more that join us, the more we give"
      name: 'tagline',
      label: "Tagline",
      type: 'text',
      required: true,
      localized: true,
      admin: {
        description: "What this project is about, in as few words as possible",
      }
    },
    { // example "The Snowdrift.coop team of volunteers"
      name: 'who',
      label: "Who",
      type: 'text',
      required: true,
      localized: true,
      admin: {
        description: "The names of the person(s) representing/running the project",
      }
    },
    {
      name: 'creator',
      label: "Creator",
      type: 'relationship',
      required: true,
      relationTo: 'users',
      admin: {
        position: 'sidebar',
        description: 'The user who created this project',
      },
      // defaultValues can use functions to return data to populate the create form and also when making POST requests the server will use the value or function to fill in any undefined fields before validation occurs
      defaultValue: ({ user }) => (user.id),
    },
    {
      name: 'admins',
      type: 'relationship',
      relationTo: 'users',
      required: true,
      hasMany: true, // a project can have multiple admins
      min: 1, // there must be at least one admin at all times
      defaultValue: ({ user }) => [user.id], // the creator becomes an admin
      admin: {
        description: 'Admins have complete control over the project',
      },
    },
    {
      name: 'editors',
      type: 'relationship',
      relationTo: 'users',
      hasMany: true, // a project can have multiple editors
      admin: {
        description: 'Editors can publish and manage the project\'s posts, including the posts of other users',
      },
    },
    {
      name: 'authors',
      type: 'relationship',
      relationTo: 'users',
      hasMany: true, // a project can have multiple authors
      admin: {
        description: 'Authors can publish and manage only their own posts.',
      },
    },
  ],
}








export default Projects;