import { Access } from 'payload/config';
import { CollectionConfig } from 'payload/types';

import { isAdmin, isAdminFieldLevel } from '../access/isAdmin';
import { isAdminOrSelf } from '../access/isAdminOrSelf';



export const Users: CollectionConfig = {

  slug: 'users',

  // any collection can have auth enabled, and may have more than one
  // by enabling auth, the API adds more routes for api/users like /login, /forgot-password, and more
  auth: {

    // useAPIKey will add a generated token visible to the user in the admin UI that can then be used to make API requests
    useAPIKey: true,
    tokenExpiration: 31536000, // keep logged in for a year, only on this device and browser, unless they log out or delete cookies. Reduces friction for people coming back only infrequently
    maxLoginAttempts: 10, // pretty generous, but not infinite
    lockTime: 43200000, // 12 hours, so legit users can at least come back the same day
    verify: true,
  },
  admin: {
    useAsTitle: 'email',
    group: 'Admin',
  },
  access: {
    // Anyone can create a user (email verification will protect this)
    create: () => true,

    // Admins can read all, but any other logged in user can only read themselves
    read: isAdminOrSelf,
    // Admins can update all, but any other logged in user can only update themselves
    update: isAdminOrSelf,
    // Only admins can delete
    delete: isAdmin,
    //admin: TODO allow admin access only if you have a global role or a role in any project
  },
  // auth enabled collections get email and other fields for secure authentication in addition to the fields you add
  fields: [
    {
      name: 'name',
      type: 'text',
      // saveToJWT tells Payload to include the field data to the JSON web token used to authenticate users
      saveToJWT: true,
    },
    { // This should be filled from old database when old users are migrated
      name: 'joinDate',
      type: 'date',
      admin: {
        position: 'sidebar',
        description: 'when the user became a Snowdrifter',
        readOnly: true,
      },
      defaultValue: () => (new Date()),
    },    
    {
      name: 'roles',
      label: "Global Admin Roles",
      type: 'select',
      saveToJWT: true,
      hasMany: true,
      access: {
        // Only admins can create or update a value for this field
        create: isAdminFieldLevel,
        update: isAdminFieldLevel,
      },
      // Default role is to have no global roles (general public)
      defaultValue: [],
      options: [ 
        { label : "System Administrator", value : 'admin'}, 
        { label : "Account Moderator", value : 'accountMod'}, 
        { label : "Content Moderator", value : 'contentMod'}, 
        { label : "Financial Moderator", value : 'financialMod'},
      ],
      admin : {description: "Snowdrift team members can wear these hats."}
    },
  ],
};

export default Users;